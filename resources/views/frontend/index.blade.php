@extends('frontend.master')
@section('home')


@include('frontend.home.hero-area')
<!--================================
        END HERO AREA
=================================-->

<!--======================================
        START FEATURE AREA
 ======================================-->
@include('frontend.home.feature-area')
<!--======================================
       END FEATURE AREA
======================================-->

<!--======================================
        START CATEGORY AREA
======================================-->
<!-- include('frontend.home.category-area') -->
<!--======================================
        END CATEGORY AREA
======================================-->

<!--======================================
        START COURSE AREA
======================================-->
@include('frontend.home.courses-area')
<!--======================================
        END COURSE AREA
======================================-->

<!--======================================
        START COURSE AREA
======================================-->
<!-- include('frontend.home.courses-area-two') -->
<!--======================================
        END COURSE AREA
======================================-->



<!-- <div class="section-block"></div>



<div class="section-block"></div> -->

<!--======================================
        START REGISTER AREA
======================================-->
<!-- include('frontend.home.feature-area') -->
<!--======================================
        END REGISTER AREA
======================================-->

<!-- <div class="section-block"></div> -->




@endsection
